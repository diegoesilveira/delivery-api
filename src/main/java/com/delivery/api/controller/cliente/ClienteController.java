package com.delivery.api.controller.cliente;

import com.delivery.api.controller.IController;
import com.delivery.api.controller.dto.ClienteDto;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ClienteController implements IController<ClienteDto, Long> {
    @Override
    public ResponseEntity<ClienteDto> create(ClienteDto clienteDTO) {
        return null;
    }

    @Override
    public ResponseEntity<List<ClienteDto>> findAll() {
        return null;
    }

    @Override
    public ResponseEntity<ClienteDto> findById(Long id) {
        return null;
    }

    @Override
    public ResponseEntity<ClienteDto> update(Long id, ClienteDto clienteDTO) {
        return null;
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        return null;
    }
}
