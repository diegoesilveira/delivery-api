package com.delivery.api.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntregaDto {

    private Long id;
    private PedidoDto pedido;
    private String logradouro;

}
