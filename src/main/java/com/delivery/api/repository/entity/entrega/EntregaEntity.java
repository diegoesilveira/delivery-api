package com.delivery.api.repository.entity.entrega;


import com.delivery.api.repository.entity.pedido.PedidoEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EntregaEntity {

    private Long id;

    private PedidoEntity pedido;
}
