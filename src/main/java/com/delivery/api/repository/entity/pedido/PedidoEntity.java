package com.delivery.api.repository.entity.pedido;


import com.delivery.api.repository.entity.cliente.ClienteEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PedidoEntity {

    private Long id;

    private ClienteEntity cliente;
}
