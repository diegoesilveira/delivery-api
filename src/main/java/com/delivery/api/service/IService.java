package com.delivery.api.service;

import java.util.List;

public interface IService <T, ID> {

    T create(T t);
    List<T> findByAll();

    T findById(ID id);

    T update(ID id, T t);

    void delete(ID id);
}
