package com.delivery.api.service.cliente;

import com.delivery.api.repository.entity.cliente.ClienteEntity;
import com.delivery.api.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ClienteService implements IService<ClienteEntity, Long> {
    @Override
    public ClienteEntity create(ClienteEntity clienteEntity) {
        return null;
    }

    @Override
    public List<ClienteEntity> findByAll() {
        return null;
    }

    @Override
    public ClienteEntity findById(Long id) {
        return null;
    }

    @Override
    public ClienteEntity update(Long id, ClienteEntity clienteEntity) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
