package com.delivery.api.service.entrega;

import com.delivery.api.repository.entity.entrega.EntregaEntity;
import com.delivery.api.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntregaService implements IService<EntregaEntity, Long> {

    @Override
    public EntregaEntity create(EntregaEntity entregaEntity) {
        return null;
    }

    @Override
    public List<EntregaEntity> findByAll() {
        return null;
    }

    @Override
    public EntregaEntity findById(Long id) {
        return null;
    }

    @Override
    public EntregaEntity update(Long id, EntregaEntity entregaEntity) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }
}
