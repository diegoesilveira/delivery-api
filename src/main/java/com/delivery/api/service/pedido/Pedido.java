package com.delivery.api.service.pedido;

import com.delivery.api.repository.entity.pedido.PedidoEntity;
import com.delivery.api.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class Pedido implements IService<PedidoEntity, Long> {
    @Override
    public PedidoEntity create(PedidoEntity pedidoEntity) {
        return null;
    }

    @Override
    public List<PedidoEntity> findByAll() {
        return null;
    }

    @Override
    public PedidoEntity findById(Long aLong) {
        return null;
    }

    @Override
    public PedidoEntity update(Long aLong, PedidoEntity pedidoEntity) {
        return null;
    }

    @Override
    public void delete(Long aLong) {

    }
}
